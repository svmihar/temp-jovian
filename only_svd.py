import pandas as pd
from tqdm import tqdm
import pdb  
import joblib
from cuml.manifold import TSNE
from cuml import UMAP
from sklearn.feature_extraction.text import TfidfVectorizer
from preprocess import remove_stopwords, remove_punctuation


df = pd.read_csv('https://github.com/svmihar/transformers_state_trolls_cch/raw/master/data/train_raw.csv')

df['clean_text'] = df['clean_text'].apply(remove_punctuation)
df['clean_text'] = df['clean_text'].apply(remove_stopwords)
df.dropna(inplace=True)

vectorizer = TfidfVectorizer(ngram_range=(1,2), max_features=100000)
def t(n, df=df): 
    df = df.sample(n)
    df.to_csv(f'sample/sample_umap_{n}.csv', index=False)
    pca = UMAP(n_components=2, verbose=1)
    X = vectorizer.fit_transform(df['clean_text'].values)
    X_array = X.toarray()
    X_pca = pca.fit_transform(X_array)
    joblib.dump(X_pca, f'./dr/X_UMAP_{n}.pkl')

if __name__ == "__main__":
    for i in tqdm(range(1000,len(df),1000)): 
        t(i)
